# Ray tracing in one shader

Very simple ray tracer writen as a fragment shader, based in the [Ray Tracing in one weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html) book series.

## How to run

As is, the shader can be run using my [shader viewer](https://gitlab.com/alvaro.sch/shader-viewer) project, since there are no binary releases for now, manual compilation will be needed, the most straight-forward way to run is as follows.

```sh
cargo run --release -- -c 4.1 /path/to/ray-tracing.frag
```

It's also possible to change a few things for this to be able to run somewhere else like [shadertoy](https://www.shadertoy.com/), as it's also possible to completely rewrite the whole thing in a programming language that writes to a ppm image.
