#version 410

layout(location = 0) out vec4 frag_color;

uniform float delta;
uniform float elapsed;
uniform ivec2 screen;
uniform ivec2 mouse;

#define MSAA_SAMPLES 4

const float PI = 3.14159274;
const float INFTY = 1.0 / 0.0;
const float THETA = 2.0 * PI / MSAA_SAMPLES;

vec2 uv = gl_FragCoord.xy / vec2(screen);

struct ray_t {
    vec3 origin;
    vec3 dir;
};

struct camera_t {
    vec3 origin;
    vec3 horizontal;
    vec3 vertical;
    vec3 lower_left;
};

camera_t camera_create(vec3 origin, float aspect, float viewport_height, float focal_length) {
    float viewport_width = aspect * viewport_height;

    camera_t cam;
    cam.origin = origin;
    cam.horizontal = vec3(viewport_width, 0.0, 0.0);
    cam.vertical = vec3(0.0, viewport_height, 0.0);
    cam.lower_left = origin - cam.horizontal / 2.0 - cam.vertical / 2.0 - vec3(0.0, 0.0, focal_length);

    return cam;
}

ray_t camera_get_ray(in camera_t cam, vec2 uv) {
    return ray_t(cam.origin, normalize(cam.lower_left + uv.x * cam.horizontal + uv.y * cam.vertical - cam.origin));
}

struct sphere_t {
    vec3 center;
    float radius;
};

struct collision_t {
    vec3 point;
    vec3 normal;
    float t;
};

sphere_t spheres[] = sphere_t[](
    sphere_t(vec3(0.0, 0.0, -1.0), 0.4),
    sphere_t(vec3(0.0, -100.5, -1.0), 100.0)
);

bool hit_sphere(in ray_t ray, in sphere_t sphere, vec2 acc_range, out collision_t hit) {
    vec3 u = ray.origin - sphere.center;

    float a = dot(ray.dir, ray.dir);
    float v = dot(ray.dir, u);
    float c = dot(u, u) - sphere.radius * sphere.radius;

    float discriminant = v * v - a * c;

    if (discriminant < 0) {
        return false;
    }

    float sqrdisc = sqrt(discriminant);

    float root = (-v - sqrdisc) / a;
    if (root < acc_range.x || acc_range.y < root) {
        root = (-v + sqrdisc) / a;
        if (root < acc_range.x || acc_range.y < root) {
            return false;
        }
    }

    hit.t = root;
    hit.point = ray.origin + hit.t * ray.dir;
    hit.normal = (hit.point - sphere.center) / sphere.radius;
    return true;
}

bool min_hit(in ray_t ray, vec2 acc_range, out collision_t hit) {
    bool found_hit = false;

    collision_t min_hit, temp_hit;
    min_hit.t = INFTY;

    for (uint i = 0; i < spheres.length(); ++i) {
        if (hit_sphere(ray, spheres[i], acc_range, temp_hit)) {
            found_hit = true;

            if (temp_hit.t < min_hit.t) {
                min_hit = temp_hit;
            }
        }
    }

    if (found_hit) {
        hit = min_hit;
    }

    return found_hit;
}

void main() {
    float aspect = float(screen.x / screen.y);

    const vec3 origin = vec3(0.0);
    const float viewport_height = 2.0;
    const float focal_length = 1.0;
    camera_t camera = camera_create(origin, aspect, viewport_height, focal_length);

    vec3 color = vec3(0.0);

    for (uint i = 0; i < MSAA_SAMPLES; ++i) {
        vec2 offset = vec2(0.0);
        if (MSAA_SAMPLES > 1) {
            float angle = i * THETA;
            offset = 0.0005 * vec2(cos(angle), sin(angle));
        }

        ray_t ray = camera_get_ray(camera, uv + offset);

        collision_t collision_record;
        if (min_hit(ray, vec2(0.0, INFTY), collision_record)) {
            color += 0.5 * (collision_record.normal + vec3(1.0));
        } else {
            float t = 0.5 * (ray.dir.y + 1.0);
            color += mix(vec3(1.0), vec3(0.5, 0.7, 1.0), t);
        }
    }

    color /= MSAA_SAMPLES;

    frag_color = vec4(color, 1.0);
}
